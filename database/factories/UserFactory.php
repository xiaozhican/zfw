<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'username'=>$faker->userName,
        'truename'=>$faker->name(),
        'password'=>bcrypt('123456'),
        'email'=>$faker->email,
        'phone'=>$faker->phoneNumber,
        'sex'=>['男','女'][rand(0,1)]
    ];
});
