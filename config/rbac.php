<?php
return [
    'super'=>'admin',//超级管理员
    //不验证的路由
    'allow_route'=>[
        'admin.index',
        'admin.welcome',
        'admin.logout',
    ]
];
