<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body{
            font-size: 14px;
        }
        div{
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<div>你的账号：{{$model->username}}</div>
<div>你的密码：{{$pwd}}</div>
<div>你的真实姓名：{{$model->truename}}</div>
<div>你的手机号码：{{$model->phone}}</div>
</body>
</html>
