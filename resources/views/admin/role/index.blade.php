<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/admin/static/h-ui/css/H-ui.min.css"/>
    <link rel="stylesheet" type="text/css" href="/admin/static/h-ui.admin/css/H-ui.admin.css"/>
    <link rel="stylesheet" type="text/css" href="/admin/lib/Hui-iconfont/1.0.8/iconfont.css"/>
    <link rel="stylesheet" type="text/css" href="/admin/static/h-ui.admin/skin/default/skin.css" id="skin"/>
    <link rel="stylesheet" type="text/css" href="/admin/static/h-ui.admin/css/style.css"/>
    <title>角色列表</title>
</head>
<body>
@extends('admin.common.main')
@section('cnt')
    <nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 用户管理 <span
            class="c-gray en">&gt;</span> 角色列表 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px"
                                                  href="javascript:location.replace(location.href);" title="刷新"><i
                class="Hui-iconfont">&#xe68f;</i></a></nav>
    @include('admin.common.msg')
    <div class="page-container">
        <form method="get" class="text-c"> 角色名：
            <input type="text" class="input-text" style="width:250px" placeholder="角色" id="" name="name" value="{{$name}}">
            <button type="submit" class="btn btn-success radius" id="" name=""><i class="Hui-iconfont">&#xe665;</i> 搜角色
            </button>
        </form>
        <div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l">
            <a href="{{route('admin.role.create')}}" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i> 添加角色</a></span>
        </div>
        <div class="mt-20">
            <table class="table table-border table-bordered table-hover table-bg table-sort">
                <thead>
                <tr class="text-c">
                    <th width="80">ID</th>
                    <th width="100">角色名称</th>
                    <th width="100">查看权限</th>
                    <th width="130">加入时间</th>
                    <th width="100">操作</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $item)
                    <tr class="text-c">
                        <td>{{$item->id}}</td>
                        <td>{{$item->name}}</td>
                        <td>
                            <a href="{{route('admin.role.node',$item)}}" class="label label-success radius">权限</a>
                        </td>
                        <td>{{$item->created_at}}</td>
                        <td class="td-manage">
                            <a href="{{route('admin.role.edit',$item)}}" class="label label-success radius">修改</a>
                            <a href="{{route('admin.role.destroy',['id'=>$item->id])}}"
                               class="label label-warning radius">删除</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$data->links()}}
        </div>
    </div>
    @endsection
@section('js')
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/admin/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/admin/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/admin/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/admin/static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/admin/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/admin/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/admin/lib/laypage/1.2/laypage.js"></script>
<script>
    const _token = "{{csrf_token()}}";
    $('.delbtn').click(function () {
        let url = $(this).attr('href');
        $.ajax({
            url: url,
            type: 'delete',
            data: {_token},
            dataType: 'json'
        }).then(({status, msg}) => {
            if (status == 0) {
                layer.msg(msg, {time: 2000, icon: 1}, () => {
                    $(this).parents('tr').remove();
                });
            }else if(status == 1){
                layer.msg(msg);
            }
        });
        return false;
    });
    //全选删除
    function deleteAll() {
        //询问框
        layer.confirm('您是真的要删除选中的用户吗？', {
            btn: ['确认删除','取消删除'] //按钮
        }, ()=>{
            let ids = $('input[name="id[]"]:checked');
            let id = [];
            $.each(ids,(key,val)=>{
                //dom对象转为jquery对象
                id.push($(val).val());
            });
            //console.log(id);
            if(id.length > 0){
                $.ajax({
                    url:"{{route('admin.user.delall')}}",
                    data:{id,_token},
                    type:'delete',
                }).then(ret=>{
                    if(ret.status == 0){
                        layer.msg(ret.msg,{time:2000,icon:1},()=>{
                            location.reload();
                        })
                    }
                });
            }
            //layer.msg('的确很重要', {icon: 1});
        });
    }
</script>
@endsection
