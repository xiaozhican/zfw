@extends('admin.common.main')
@section('css')
    <!--引入CSS-->
    <link rel="stylesheet" type="text/css" href="/webuploader/webuploader.css">
@endsection
@section('cnt')
    <nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 文章管理 <span class="c-gray en">&gt;</span> 添加文章 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
    <article class="page-container">
        @include('admin.common.validate')
{{--        enctype="multipart/form-data"--}}
        <form action="{{route('admin.article.store')}}" method="post" class="form form-horizontal">
            @csrf
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>文章标题：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text"  name="title" autocomplete="off">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>文章摘要：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text"  name="desn" autocomplete="off">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>文章封面：</label>
                <div class="formControls col-xs-4 col-sm-1">
{{--                    <input type="file" class="input-text"  name="pic" autocomplete="off">--}}
                    <input type="hidden" name="pic" id="pic" value="{{config('up.pic')}}">
                    <div id="picker">上传文章封面</div>
                </div>
                <div class="formControls col-xs-4 col-sm-8">
                    <img src="" id="img" alt="" style="width:100px">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>文章内容：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <textarea name="body" id="body" cols="30" rows="10"></textarea>
                </div>
            </div>
            <div class="row cl">
                <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
                    <input class="btn btn-primary radius" type="submit" value="添加文章">
                </div>
            </div>
        </form>
    </article>
@endsection
@section('js')
    <!--引入JS-->
    <script type="text/javascript" src="/webuploader/webuploader.js"></script>
    <!-- 配置文件 -->
    <script type="text/javascript" src="/ueditor/ueditor.config.js"></script>
    <!-- 编辑器源码文件 -->
    <script type="text/javascript" src="/ueditor/ueditor.all.js"></script>
    <!-- 实例化编辑器 -->
    <script type="text/javascript">
        //富文本编辑器
        var ue = UE.getEditor('body',{
            initialFrameHeight:400
        });
        var uploader = WebUploader.create({
            //自动上传
            auto:true,
            // swf文件路径
            swf: '/webuploader/Uploader.swf',
            // 文件接收服务端。
            server: "{{route('admin.article.upfile')}}",
            formData:{
                _token:"{{csrf_token()}}"
            },
            fileVal:'file',//文件上传的表单名称，默认是file
            // 选择文件的按钮。可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: {
                id:'#picker',
                multiple:false,//是否开启多个文件上传
            },
            // 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
            resize: true
        });
        //文件上传成功的回调函数
        uploader.on( 'uploadSuccess', function( file,ret) {
            //$( '#'+file.id ).find('p.state').text('已上传');
            let src = ret.url;
            $('#pic').val(src);
            $("#img").attr('src',src);
        });
    </script>
@endsection
