@extends('admin.common.main')
@section('cnt')
    <nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 文章管理 <span
            class="c-gray en">&gt;</span> 文章列表 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px"
                                                  href="javascript:location.replace(location.href);" title="刷新"><i
                class="Hui-iconfont">&#xe68f;</i></a></nav>
    @include('admin.common.msg')
    <div class="page-container">
        <form method="get" class="text-c" onsubmit="return dopost()">
            文章标题：
            <input type="text" class="input-text" style="width:250px" placeholder="文章标题" id="title" value="{{request()->get('title')}}">
            时间选择：
            <input type="text" onfocus="WdatePicker({ maxDate:'#F{$dp.$D(\'datemax\')||\'%y-%M-%d\'}' })" id="datemin"
                   class="input-text Wdate" style="width:120px;">
            -
            <input type="text" onfocus="WdatePicker({ minDate:'#F{$dp.$D(\'datemin\')}',maxDate:'%y-%M-%d' })" id="datemax"
                   class="input-text Wdate" style="width:120px;">
            <button type="submit" class="btn btn-success radius" id="" name=""><i class="Hui-iconfont">&#xe665;</i> 查询
            </button>
        </form>
        <div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l">
            <a href="{{route('admin.article.create')}}" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i> 添加文章</a></span>
        </div>
        <div class="mt-20">
            <table class="table table-border table-bordered table-hover table-bg table-sort">
                <thead>
                <tr class="text-c">
                    <th width="80">ID</th>
                    <th width="100">文章标题</th>
                    <th width="130">加入时间</th>
                    <th width="100">操作</th>
                </tr>
                </thead>

            </table>
        </div>
    </div>
@endsection
@section('js')
    <script>
        //客户端分页
        var dataTable = $('.table-sort').DataTable({
            //lengthMenu:[5,10,15,20,50],//下拉分页数
            searching:false,//隐藏搜索
            columnDefs:[
                {targets:[3],orderable:false}
            ],
            //开启服务端分页，开启ajax
            serverSide:true,
            //进行ajax请求
            ajax:{
                url:"{{route('admin.article.index')}}",
                type:'get',
                data:function (ret) {
                    ret.datemin = $('#datemin').val();
                    ret.datemax = $('#datemax').val();
                    ret.title = $.trim($('#title').val());
                }
            },
            //指定每一列显示的数据
            columns:[
                {'data':'id',className:'text-c'},
                {'data':'title',className:'text-c'},
                {'data':'created_at',className:'text-c'},
                {'data':'actions',defaultContent:'',className:'text-c'},
            ],
            //回调方法  action改为其他的
            //row 当前行的dom对象
            //data 当前行的数据
            //dataIndex 当前行的数据索引
            createdRow:function (row,data,dataIndex) {
                //当前id
                var id = data.id;
                //行的最后一列
                var td = $(row).find('td:last-child');
                //显示html内容
                var html = `
                <a href="/admin/article/${id}/edit" class="label label-success radius">修改</a>
                <a href="/admin/article/${id}" onclick="return delArticle(event,this)" class="label label-warning radius">删除</a>
                `;
                td.html(html);
            }
        });
        //表单提交
        function dopost() {
            dataTable.ajax.reload();
            return false;
        }
        async function delArticle(evt,obj) {
            evt.preventDefault();
            let url = $(obj).attr('href');
            let ret = await fetch(url,{
                method:"delete",
                headers:{
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                },
                //body:'aa=aa&bb=bb'
            });
            let json = await ret.json();
            console.log(json);
            return false;
        }
        function delArticle2(obj) {
            let url = $(obj).attr('href');
            fetch(url,{
                method:"delete",
                headers:{
                    'X-CSRF-TOKEN':'{{csrf_token()}}'
                },
                //body:'aa=aa&bb=bb'
            }).then(res=>{
                return res.json();
            }).then(data=>{
                console.log(data);
            });
            return false;
        }
    </script>
@endsection
