@extends('admin.common.main')
@section('cnt')
    <nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 用户管理 <span class="c-gray en">&gt;</span> 添加用户 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
    <article class="page-container">
        @include('admin.common.validate')
        <form action="{{route('admin.user.store')}}" method="post" class="form form-horizontal" id="form-member-add">
            @csrf
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>账号：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text"  name="username" autocomplete="off" value="{{old('username')}}">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>真实姓名：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text"  name="truename" autocomplete="off" value="{{old('truename')}}">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>密码：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" id="password" name="password" autocomplete="off">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>确认密码：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text"  name="password_confirmation" autocomplete="off">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>性别：</label>
                <div class="formControls col-xs-8 col-sm-9 skin-minimal">
                    <div class="radio-box">
                        <input name="sex" type="radio" value="男" checked>
                        <label for="sex-1">男</label>
                    </div>
                    <div class="radio-box">
                        <input type="radio" value="女" name="sex">
                        <label for="sex-2">女</label>
                    </div>
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>手机：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="" placeholder="" name="phone" autocomplete="off" value="{{old('phone')}}">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>邮箱：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="email" class="input-text" placeholder="" name="email" id="email" autocomplete="off" value="{{old('email')}}">
                </div>
            </div>

            <div class="row cl">
                <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
                    <input class="btn btn-primary radius" type="submit" value="添加用户">
                </div>
            </div>
        </form>
    </article>
@endsection
@section('js')
    <script src="/admin/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
    <script src="/admin/lib/jquery.validation/1.14.0/validate-methods.js"></script>
    <script src="/admin/lib/jquery.validation/1.14.0/messages_zh.js"></script>

    <script>
        $('.skin-minimal input').iCheck({
            checkboxClass: 'icheckbox-blue',
            radioClass: 'iradio-blue',
            increaseArea: '20%'
        });
        //https://www.runoob.com/jquery/jquery-plugin-validate.html
        // $("#form-member-add").validate({
        //     rules:{
        //         username:{
        //             required:true,
        //             minlength:2,
        //             maxlength:16
        //         },
        //         truename:{
        //             required:true
        //         },
        //         password:{
        //             required:true
        //         },
        //         password_confirmation:{
        //             equalTo:"#password"
        //         },
        //         sex:{
        //             required:true,
        //         },
        //         phone:{
        //             required:true,
        //             phone:true
        //         },
        //         email:{
        //             required:true,
        //             email:true,
        //         },
        //
        //     },
        //     onkeyup:false,
        //     success:"valid",
        //     submitHandler:function(form){
        //         form.submit();//表单提交
        //     }
        // });
        // //自定义验证规则
        // jQuery.validator.addMethod("phone", function(value, element) {
        //     var reg1 = /^\+86-1[3-9]\d{9}$/;
        //     var reg2 = /^1[3-9]\d{9}$/;
        //     var ret = reg1.test(value) || reg2.test(value);
        //     return this.optional(element) || ret;
        // }, "请输入正确的手机号码");
    </script>
@endsection
