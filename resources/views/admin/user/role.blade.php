<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
@include('admin.common.validate')
<form action="{{route('admin.user.role',$user)}}" method="post">
    @csrf
    @foreach($roleAll as $item)
        <div>
            <label>{{$item->name}}
                <input type="radio" name="role_id" value="{{$item->id}}" @if($item->id == $user->role_id) checked @endif>
            </label>
        </div>
    @endforeach
    <button type="submit">给用户分配角色</button>
</form>
</body>
</html>
