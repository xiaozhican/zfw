@extends('admin.common.main')
@section('cnt')
    <nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 用户管理 <span
            class="c-gray en">&gt;</span> 权限列表 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px"
                                                  href="javascript:location.replace(location.href);" title="刷新"><i
                class="Hui-iconfont">&#xe68f;</i></a></nav>
    @include('admin.common.msg')
    <div class="page-container">
        <form method="get" class="text-c"> 权限名：
            <input type="text" class="input-text" style="width:250px" placeholder="权限" id="" name="name" value="">
            <button type="submit" class="btn btn-success radius" id="" name=""><i class="Hui-iconfont">&#xe665;</i> 搜权限
            </button>
        </form>
        <div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l">
            <a href="{{route('admin.node.create')}}" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i> 添加权限</a></span>
        </div>
        <div class="mt-20">
            <table class="table table-border table-bordered table-hover table-bg table-sort">
                <thead>
                <tr class="text-c">
                    <th width="80">ID</th>
                    <th width="100">权限名称</th>
                    <th width="100">路由别名</th>
                    <th width="100">是否菜单</th>
                    <th width="130">加入时间</th>
                    <th width="100">操作</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $item)
                    <tr class="text-c">
                        <td>{{$item['id']}}</td>
                        <td class="text-l">
                            {{$item['html']}}
                            {{$item['name']}}
                        </td>
                        <td>{{$item['route_name']}}</td>
                        <td>
{{--                            {!! $item['menu'] !!}--}}
                            @if($item['is_menu'] == 1)
                                <span href="" class="label label-success radius">是</span>
                            @else
                                <span href="" class="label label-danger radius">否</span>
                            @endif
                        </td>
                        <td>{{$item['created_at']}}</td>
                        <td class="td-manage">
                            <a href="{{route('admin.node.edit',['id'=>$item['id']])}}" class="label label-success radius">修改</a>
                            <a href="{{route('admin.node.destroy',['id'=>$item['id']])}}"
                               class="label label-warning radius">删除</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('js')
    <script>
        const _token = "{{csrf_token()}}";
        $('.delbtn').click(function () {
            let url = $(this).attr('href');
            $.ajax({
                url: url,
                type: 'delete',
                data: {_token},
                dataType: 'json'
            }).then(({status, msg}) => {
                if (status == 0) {
                    layer.msg(msg, {time: 2000, icon: 1}, () => {
                        $(this).parents('tr').remove();
                    });
                }else if(status == 1){
                    layer.msg(msg);
                }
            });
            return false;
        });
        //全选删除
        function deleteAll() {
            //询问框
            layer.confirm('您是真的要删除选中的用户吗？', {
                btn: ['确认删除','取消删除'] //按钮
            }, ()=>{
                let ids = $('input[name="id[]"]:checked');
                let id = [];
                $.each(ids,(key,val)=>{
                    //dom对象转为jquery对象
                    id.push($(val).val());
                });
                //console.log(id);
                if(id.length > 0){
                    $.ajax({
                        url:"{{route('admin.user.delall')}}",
                        data:{id,_token},
                        type:'delete',
                    }).then(ret=>{
                        if(ret.status == 0){
                            layer.msg(ret.msg,{time:2000,icon:1},()=>{
                                location.reload();
                            })
                        }
                    });
                }
                //layer.msg('的确很重要', {icon: 1});
            });
        }
    </script>
@endsection
