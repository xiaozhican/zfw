@if(session()->has('success'))
    <div class="Huialert Huialert-success"><i class="Hui-iconfont">&#xe6a6;</i>
        {{session('success')}}
    </div>
@elseif(session()->has('error'))
    <div class="Huialert Huialert-error"><i class="Hui-iconfont">&#xe6a6;</i>
        {{session('error')}}
    </div>
@endif
