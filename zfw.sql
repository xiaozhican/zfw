/*
Navicat MySQL Data Transfer

Source Server         : Laragon
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : zfw

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2020-05-13 17:21:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for articles
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文章标题',
  `desn` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文章摘要',
  `pic` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文章封面',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文章内容',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of articles
-- ----------------------------
INSERT INTO `articles` VALUES ('31', 'aaa11111', 'aaa', '/uploads/article/2AE1onWeQ7IYf0VMeSBwTKbbGHOdPGPbXLJThOdt.jpeg', '<p>你好，PHP</p>', '2020-05-12 09:41:20', '2020-05-12 14:28:25', null);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('8', '2020_05_07_092905_create_users_table', '1');
INSERT INTO `migrations` VALUES ('9', '2020_05_09_145955_create_roles_table', '1');
INSERT INTO `migrations` VALUES ('10', '2020_05_09_150257_create_nodes_table', '1');
INSERT INTO `migrations` VALUES ('11', '2020_05_09_150516_role_node', '1');
INSERT INTO `migrations` VALUES ('12', '2020_05_11_143755_create_articles_table', '2');

-- ----------------------------
-- Table structure for nodes
-- ----------------------------
DROP TABLE IF EXISTS `nodes`;
CREATE TABLE `nodes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '节点，权限名称',
  `route_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '路由别名，权限认证标识',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `is_menu` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '是否为菜单，0否，1是',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of nodes
-- ----------------------------
INSERT INTO `nodes` VALUES ('1', '后台管理', '', '0', '1', '2020-05-10 14:48:31', '2020-05-10 14:48:31', null);
INSERT INTO `nodes` VALUES ('2', '用户列表', 'admin.user.index', '1', '1', '2020-05-10 14:56:44', '2020-05-10 14:56:44', null);
INSERT INTO `nodes` VALUES ('3', '角色列表', 'admin.role.index', '1', '1', '2020-05-10 14:57:35', '2020-05-10 14:57:35', null);
INSERT INTO `nodes` VALUES ('4', '添加用户', 'admin.user.create', '1', '0', '2020-05-10 15:43:53', '2020-05-10 15:43:53', null);
INSERT INTO `nodes` VALUES ('5', '文章管理', '', '0', '1', '2020-05-11 10:29:23', '2020-05-11 10:29:23', null);
INSERT INTO `nodes` VALUES ('6', '权限列表', 'admin.node.index', '1', '1', '2020-05-11 10:32:15', '2020-05-11 10:32:15', null);
INSERT INTO `nodes` VALUES ('7', '文章列表', 'admin.article.index', '5', '1', '2020-05-11 14:55:00', '2020-05-11 14:55:00', null);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '角色名称',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', '研发组长', '2020-05-10 10:26:07', '2020-05-10 10:26:07', null);
INSERT INTO `roles` VALUES ('2', 'ab', '2020-05-10 10:30:34', '2020-05-10 11:18:43', null);

-- ----------------------------
-- Table structure for role_node
-- ----------------------------
DROP TABLE IF EXISTS `role_node`;
CREATE TABLE `role_node` (
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `node_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '节点id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of role_node
-- ----------------------------
INSERT INTO `role_node` VALUES ('1', '1');
INSERT INTO `role_node` VALUES ('1', '2');
INSERT INTO `role_node` VALUES ('1', '3');
INSERT INTO `role_node` VALUES ('1', '4');
INSERT INTO `role_node` VALUES ('1', '6');
INSERT INTO `role_node` VALUES ('1', '5');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '账号',
  `truename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '未知' COMMENT '真实姓名',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '手机号码',
  `sex` enum('男','女') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '男' COMMENT '性别',
  `last_ip` char(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '登录ip',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '1', 'admin', 'Anastasia McClure', '$2y$10$l2YfCvF5l4xN3qKXOu3lq.1QHdJD13lrqxbmtQjxit1FqYgz2MluK', 'clubowitz@gmail.com', '(852) 270-3987', '女', '', '2020-05-09 15:25:05', '2020-05-09 15:25:13', null);
INSERT INTO `users` VALUES ('2', '2', 'ckunze', 'Chandler Yundt', '$2y$10$QcpNliLQTq91dmX9RKyGHuUOw0ZaoitxHuk7f29GM9RgfD3TKUYjG', 'hand.ola@schowalter.info', '328-723-0520 x07626', '女', '', '2020-05-09 15:25:05', '2020-05-10 16:46:26', null);
INSERT INTO `users` VALUES ('3', '1', 'cathryn18', 'Prof. Jaeden Bailey III', '$2y$10$Q9hG/yxiocaP/VR5V9rgROlaNi4rLHTEoPru56TuJbU.vovOOacGm', 'anna.willms@yahoo.com', '531.737.2172 x1153', '男', '', '2020-05-09 15:25:05', '2020-05-09 15:25:05', null);
INSERT INTO `users` VALUES ('4', '1', 'hoeger.justina', 'Ms. Alvera Reichel IV', '$2y$10$uOA/NBtqwBfw64J02XHmpuo7VBpPvz0ggxaWuZmmYgZf1JPOKNdRy', 'wiza.retta@yahoo.com', '893-517-4611 x9209', '女', '', '2020-05-09 15:25:06', '2020-05-09 15:25:06', null);
INSERT INTO `users` VALUES ('5', '1', 'marian87', 'Hazel Quigley', '$2y$10$xZV4od.D8Fi69cuUfCkJPu.8Q6hFSzdAaJux261hy0nEhn5kDpOS6', 'xmayert@wintheiser.com', '1-538-999-2832', '男', '', '2020-05-09 15:25:06', '2020-05-09 15:25:06', null);
INSERT INTO `users` VALUES ('6', '1', 'ggottlieb', 'Prof. Rosalyn Hamill', '$2y$10$Zk7yKKMCVRW.3qsilhFEoOokVM9uimO9Z32XipHkcwtjYB.tneXc6', 'wgislason@yahoo.com', '1-556-424-7614', '男', '', '2020-05-09 15:25:07', '2020-05-09 15:25:07', null);
INSERT INTO `users` VALUES ('7', '1', 'hrunolfsdottir', 'Cydney Yundt DDS', '$2y$10$sr9Roq3epXmvY9C2mRWERuby5d6AaPP7Jzz8Eg.SEPLtXll6BkLly', 'ohara.otis@dietrich.com', '(817) 593-6241', '女', '', '2020-05-09 15:25:08', '2020-05-09 15:25:08', null);
INSERT INTO `users` VALUES ('8', '1', 'ardith57', 'Mr. Cletus Mertz DVM', '$2y$10$Q.wPx.sMaELqsa5o1jKQ9.9V1Q5FySIoRjvrOxlcji3jbE6gvm/VS', 'donnell94@yundt.biz', '(979) 539-7993', '女', '', '2020-05-09 15:25:08', '2020-05-09 15:25:08', null);
INSERT INTO `users` VALUES ('9', '1', 'draynor', 'Tamia Wisozk', '$2y$10$/Pl02aka0vEvBITNfNV7lOczuWvkADKqP2cg5kPDMGg2cWMZbUhn.', 'aidan98@hotmail.com', '432-596-4709', '男', '', '2020-05-09 15:25:08', '2020-05-09 15:25:08', null);
INSERT INTO `users` VALUES ('10', '1', 'kutch.narciso', 'Prof. Efren Prosacco I', '$2y$10$QYa52JpTPtJLghygon/DoucNhZ9VN.Ba4zffsK1UOGEoFD/hjpGGW', 'kaylie.beier@miller.com', '576.359.5916 x100', '女', '', '2020-05-09 15:25:09', '2020-05-09 15:25:09', null);
INSERT INTO `users` VALUES ('11', '1', 'murray.zelma', 'Cleora Shields Jr.', '$2y$10$amckSAuAI.QduzI9rwxxbuMGYZZqQnRJa0Eo9gmGzBfmMNsDTDb0m', 'ulemke@gmail.com', '1-771-947-0815 x8129', '女', '', '2020-05-09 15:25:09', '2020-05-09 15:25:09', null);
INSERT INTO `users` VALUES ('12', '1', 'ahamill', 'Dr. Courtney Krajcik', '$2y$10$O112IZSgJ.0WvRJCc.u.tufBH1Lp24wPj2/H4k3jnZK3K224nUnfO', 'shields.sister@yahoo.com', '(265) 816-9781', '女', '', '2020-05-09 15:25:09', '2020-05-09 15:25:09', null);
INSERT INTO `users` VALUES ('13', '1', 'pagac.tevin', 'Dr. Elisha Carroll', '$2y$10$Dm9hipxzdEjWyb3HynBOnOB8MaZQfGmh5huVHYOo5ksWUdB5SaZP2', 'geraldine00@spencer.org', '493.887.7419 x352', '女', '', '2020-05-09 15:25:09', '2020-05-09 15:25:09', null);
INSERT INTO `users` VALUES ('14', '1', 'madaline44', 'Felicita Wisozk V', '$2y$10$OlKFCxysbyoIfSQxrFhfH.zZ48ebmujbq92ATjiLyX75bXCkBIRtq', 'mackenzie.wunsch@gmail.com', '579-534-0237 x9183', '女', '', '2020-05-09 15:25:09', '2020-05-09 15:25:09', null);
INSERT INTO `users` VALUES ('15', '1', 'mrohan', 'Vivianne O\'Kon', '$2y$10$4433zlhkJJ6xN0t/u4GPWOHX6z2GafimZBH7A/Pl1qAZ4PVN4I1dy', 'cale.kutch@gmail.com', '1-581-701-5646', '男', '', '2020-05-09 15:25:09', '2020-05-09 15:25:09', null);
INSERT INTO `users` VALUES ('16', '1', 'kfunk', 'Mrs. Verla Kertzmann', '$2y$10$y3/EBw0sO1D/Hk1qmQU8MO53WVtkuquKsQE0F8Zr35t3U2rW8mv1m', 'katrina.morar@gmail.com', '+1 (962) 707-5469', '女', '', '2020-05-09 15:25:09', '2020-05-09 15:25:09', null);
INSERT INTO `users` VALUES ('17', '1', 'werner.towne', 'Mr. Damion VonRueden', '$2y$10$U0pcQ31sm8Np1wYFks3Y1.T0kO/SWfYKkrc1fdrpkig6bKarZK3re', 'bbode@gmail.com', '796-774-1815', '男', '', '2020-05-09 15:25:09', '2020-05-09 15:25:09', null);
INSERT INTO `users` VALUES ('18', '1', 'caltenwerth', 'Marcelina Abbott', '$2y$10$yzfN5u/v6ul5UmhjHe480udpXMFrS6EQ/Pm6aFPGgkFJxk8QQYuEa', 'adriana.rowe@graham.info', '(585) 333-2734', '女', '', '2020-05-09 15:25:10', '2020-05-09 15:25:10', null);
INSERT INTO `users` VALUES ('19', '1', 'goyette.jewel', 'Dr. Ashleigh Johnson Sr.', '$2y$10$Vf/dr0/SFUGGw3NyUboNsuuRUJbIfftK56yu1bEmnd/WpDiBSk1tC', 'cassin.gayle@marks.com', '656.582.7021 x97714', '男', '', '2020-05-09 15:25:10', '2020-05-09 15:25:10', null);
INSERT INTO `users` VALUES ('20', '1', 'gconn', 'Mrs. Priscilla Hagenes I', '$2y$10$F2zc7A8PPAVzuigju9uUmugbOJKY1xKFdiahLsMLsp8gubQ1VfjrO', 'dean.watsica@klocko.info', '+1-752-730-5218', '女', '', '2020-05-09 15:25:10', '2020-05-09 15:25:10', null);
INSERT INTO `users` VALUES ('21', '1', 'nbahringer', 'Mr. Rogelio Mills', '$2y$10$cqVK4giGInwDPdj2s9eaiuGcBXrRHj3pvtIKN5e2UirYTNbis6rw6', 'marian28@yahoo.com', '271.914.7851', '男', '', '2020-05-09 15:25:10', '2020-05-09 15:25:10', null);
INSERT INTO `users` VALUES ('22', '1', 'santiago.green', 'Miss Lilly Schimmel', '$2y$10$RPxuGBm8GzUml0Pucd1HB.sXeGCMQCoZZHLw5lELOKut.7I.9aa/a', 'larry25@larson.net', '1-435-474-9016', '女', '', '2020-05-09 15:25:10', '2020-05-09 15:25:10', null);
INSERT INTO `users` VALUES ('23', '1', 'santa.gottlieb', 'Jackson Keebler MD', '$2y$10$74VXssCO2BsQXt4ljnvyH.hrqYC/2N4qhF/Oqn3HX5o/vpxtnBA76', 'yterry@hotmail.com', '+1 (275) 737-8324', '女', '', '2020-05-09 15:25:10', '2020-05-09 15:25:10', null);
INSERT INTO `users` VALUES ('24', '1', 'ngoyette', 'Verdie Hane', '$2y$10$HkGnnXUyitzpoD1.qWe8aONBuZPFFninHJclkJTsEuyFYWJBX311G', 'dale43@yahoo.com', '990.604.4572 x9826', '男', '', '2020-05-09 15:25:10', '2020-05-09 15:25:10', null);
INSERT INTO `users` VALUES ('25', '1', 'destin.cole', 'Conrad Pfeffer', '$2y$10$FP5Sbg99euI.zPS/TObAOuCMuxgoy7fyEQ4ojKtZLFmG6AZP4GnPW', 'vziemann@nicolas.net', '1-535-951-6686', '男', '', '2020-05-09 15:25:10', '2020-05-09 15:25:10', null);
INSERT INTO `users` VALUES ('26', '1', 'jerde.pablo', 'Milford Adams', '$2y$10$yrRzbvdg3inEKhV/p1sxuOeYJnHOhCMcC9kPz5.BAqvMv9GNNvF2i', 'nayeli08@hotmail.com', '(971) 422-2806', '男', '', '2020-05-09 15:25:10', '2020-05-09 15:25:10', null);
INSERT INTO `users` VALUES ('27', '1', 'uwelch', 'Marlee Skiles', '$2y$10$hZBACovNUHwRQQwXv9ftD.WkPlPvMMW7SA2yGHftG3mnj0HRdKz2i', 'lsimonis@hotmail.com', '(973) 856-5731', '女', '', '2020-05-09 15:25:10', '2020-05-09 15:25:10', null);
INSERT INTO `users` VALUES ('28', '1', 'yromaguera', 'Suzanne Luettgen', '$2y$10$y/NXrzGS4WfbUQUF7U3QB.sd/L4pLnnSOYHhJKOky46j/9J9zM6/K', 'titus48@gmail.com', '574.928.2460 x29589', '女', '', '2020-05-09 15:25:10', '2020-05-09 15:25:10', null);
INSERT INTO `users` VALUES ('29', '1', 'luther.marks', 'Dr. Ramona Connelly', '$2y$10$wr5v3mOXuBNDIijz.Jvw/.lHxVHiSyTZtnLdAssmXQbMMrLIHQuVe', 'rico81@gmail.com', '(984) 652-4646 x391', '女', '', '2020-05-09 15:25:10', '2020-05-09 15:25:10', null);
INSERT INTO `users` VALUES ('30', '1', 'borer.sheldon', 'Jessy Mann', '$2y$10$feiYVcsGEhZIyvADIP3GKuzJ/Yk/gLs4TrCn7mkBbphOWgh.7ouji', 'jamil88@hotmail.com', '1-434-673-8170 x274', '女', '', '2020-05-09 15:25:11', '2020-05-09 15:25:11', null);
INSERT INTO `users` VALUES ('31', '1', 'rosie79', 'Ms. Rebeka Lockman I', '$2y$10$I//FJL0dfgwl62LfwtMDY.foFDPsdHIx0gCFTCRfQy7gKFEPswG6G', 'jennyfer.spinka@mcglynn.biz', '+1.857.404.1368', '男', '', '2020-05-09 15:25:11', '2020-05-09 15:25:11', null);
INSERT INTO `users` VALUES ('32', '1', 'bernadine78', 'Ciara Will IV', '$2y$10$XJ4BeaIro2rbW4z9Vi3qaesQ53gSkP0/aeHdPRF92S2Q9XN5M8fmm', 'trycia16@willms.net', '(991) 827-5204', '男', '', '2020-05-09 15:25:11', '2020-05-09 15:25:11', null);
INSERT INTO `users` VALUES ('33', '1', 'loyce.kshlerin', 'Roxanne Padberg', '$2y$10$hIdKU7MMafktybNDsAQXT.EFu9MdDzfym9.CHfRCSUU53KpY5hAAW', 'wortiz@aufderhar.com', '1-275-947-4247 x62990', '女', '', '2020-05-09 15:25:11', '2020-05-09 15:25:11', null);
INSERT INTO `users` VALUES ('34', '1', 'raleigh16', 'Margret Pouros IV', '$2y$10$JFU5/HPoptISjzDA54c1ju7u..VKaK.FagJsA1ksV5XcaMfBXsAGG', 'ewell08@reynolds.info', '385.814.4787 x980', '女', '', '2020-05-09 15:25:11', '2020-05-09 15:25:11', null);
INSERT INTO `users` VALUES ('35', '1', 'jreynolds', 'Emmet Parisian III', '$2y$10$zk6pAltXCoKW6Fl51g2WletQeJwX2MQPa0XHh.4S3BGGPZpEVB10i', 'chasity49@lebsack.com', '460-404-9260 x9942', '女', '', '2020-05-09 15:25:11', '2020-05-09 15:25:11', null);
INSERT INTO `users` VALUES ('36', '1', 'jayda27', 'Colleen Hane', '$2y$10$wYi7VH.seR8mK027wmWGmuEHtujTuNOjD8BJ5KxFZ9tqhyPiuELZO', 'maddison83@graham.org', '+1 (483) 332-4200', '女', '', '2020-05-09 15:25:11', '2020-05-09 15:25:11', null);
INSERT INTO `users` VALUES ('37', '1', 'zhartmann', 'Mae Reinger', '$2y$10$VyvgXfGjf3PgmpgpIEZdzeYDa6exWE4hvnXJU4zOxWR2LHwZjZ.pG', 'purdy.cassandre@hotmail.com', '(774) 338-4654', '男', '', '2020-05-09 15:25:11', '2020-05-09 15:25:11', null);
INSERT INTO `users` VALUES ('38', '1', 'gohara', 'Jalyn Bradtke', '$2y$10$OXLQewkjcTR6.K8cMFcCvequ62.7d8EACyyddrj1W7cCBQEVl9wKm', 'alek00@hotmail.com', '+1 (357) 616-2712', '女', '', '2020-05-09 15:25:11', '2020-05-09 15:25:11', null);
INSERT INTO `users` VALUES ('39', '1', 'cfeil', 'Mazie Kunde', '$2y$10$blWBbCqp5VaEHe8o.BNv6uTiHIwV8Wku6.bkgGy3msifDFjR.Ijma', 'usatterfield@stroman.biz', '361-322-5905', '女', '', '2020-05-09 15:25:12', '2020-05-09 15:25:12', null);
INSERT INTO `users` VALUES ('40', '1', 'gretchen81', 'Janie Hansen', '$2y$10$DFcLUeKYti9oixTa.F/dcOLR3M3VBwAfuV2djlmaawzXionWiJ9em', 'gsimonis@kshlerin.com', '1-390-946-8599', '女', '', '2020-05-09 15:25:12', '2020-05-09 15:25:12', null);
INSERT INTO `users` VALUES ('41', '1', 'maci68', 'Shanie Keeling', '$2y$10$8h0WYsMCEEMuRdl4dL.80eZ8hCJYyFdGlOP8MsdZQZaN3RDe..pKK', 'goldner.trystan@kautzer.com', '1-301-970-6234', '女', '', '2020-05-09 15:25:12', '2020-05-09 15:25:12', null);
INSERT INTO `users` VALUES ('42', '1', 'hyman.damore', 'Charlene Hodkiewicz', '$2y$10$XypUbsyCySyP3Xx7lJPiV.V/GVS1NbvYI/mM98UFfTmUfwe3o/M6W', 'zullrich@pollich.com', '1-331-594-8499 x8028', '男', '', '2020-05-09 15:25:12', '2020-05-09 15:25:12', null);
INSERT INTO `users` VALUES ('43', '1', 'aborer', 'Freeman Wisoky Jr.', '$2y$10$qJbxuC.a/LKoBeqWjt0Sfuqe1ftW3rb1tWg/EDFGTvLjD59Ijj.C.', 'nzboncak@zieme.org', '+19599483305', '女', '', '2020-05-09 15:25:12', '2020-05-09 15:25:12', null);
INSERT INTO `users` VALUES ('44', '1', 'glennie14', 'Bryce Russel', '$2y$10$PDN9k9kCa4AtV1htq2VgdunDtTeOYgtKt1WQ0IrKG8jDfWHYXcbFy', 'river.grant@hotmail.com', '(737) 433-2683', '女', '', '2020-05-09 15:25:12', '2020-05-09 15:25:12', null);
INSERT INTO `users` VALUES ('45', '1', 'evert.koss', 'Christian Gleason II', '$2y$10$VdIB9peyQSVKJ7vjKv4wAO36z3HRzmmSIXjRt2aE7yenIa5yDqtOK', 'heaney.scot@bashirian.net', '707.984.0702', '男', '', '2020-05-09 15:25:12', '2020-05-09 15:25:12', null);
INSERT INTO `users` VALUES ('46', '1', 'braun.davion', 'Maymie Reilly', '$2y$10$MCrVdSzdiVR3RGZ3u4aX9eTWAccSY6nT/xUWqX39jpAiZHsoAn7BG', 'wilderman.nathen@yahoo.com', '442.493.3054', '男', '', '2020-05-09 15:25:12', '2020-05-09 15:25:12', null);
INSERT INTO `users` VALUES ('47', '1', 'wade.waters', 'Carole Nitzsche', '$2y$10$cvSNE4wajqvlYt/NClCkmevQIU7VmhEuaBCTh0yKN9aJ3VPjLXiiO', 'nicklaus05@weissnat.net', '969.327.3969 x10782', '男', '', '2020-05-09 15:25:12', '2020-05-09 15:25:12', null);
INSERT INTO `users` VALUES ('48', '1', 'angie23', 'Tyreek Corkery IV', '$2y$10$G06wIsoq6jBllUPzWKNDve.gwW/.V0MFeV1xPJJlWoSrTQOvt5yCq', 'toy.meaghan@toy.com', '(805) 692-2767 x32240', '女', '', '2020-05-09 15:25:12', '2020-05-09 15:25:12', null);
INSERT INTO `users` VALUES ('49', '1', 'larson.georgianna', 'Billie Weimann', '$2y$10$DSsZ/Nd59BF0AbTcJXDVUeG1LOyxHQW1S9ByYPpEof2hwK3UjPoVG', 'carlie55@boehm.com', '229-507-1256', '男', '', '2020-05-09 15:25:12', '2020-05-09 15:25:12', null);
INSERT INTO `users` VALUES ('50', '1', 'liana.stroman', 'Domenic Schulist', '$2y$10$5DNQi8bn8bUSNpDppGi/juNgtrJg8N6wck461SmyrT8.mnD1hTC8C', 'gislason.eula@gmail.com', '(493) 785-7547 x7750', '男', '', '2020-05-09 15:25:13', '2020-05-09 15:25:13', null);
