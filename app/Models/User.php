<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as AuthUser;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\Btn;

class User extends AuthUser
{
    use SoftDeletes,Btn;
    protected $dates = ['deleted_at'];
    protected $guarded = [];
    //隐藏密码
    protected $hidden = ['password'];
    //角色
    public function role()
    {
        return $this->belongsTo(Role::class,'role_id');
    }
}
