<?php

namespace App\Models;


class Node extends Base
{
    //修改器
    public function setRouteNameAttribute($value)
    {
        //如果字段值为null，则设置为空，修改和添加生效
        $this->attributes['route_name'] = empty($value)?'':$value;
    }

    public function getMenuAttribute()
    {
        if($this->is_menu == '1'){
            return '<span href="" class="label label-success radius">是</span>';
        }
        return '<span href="" class="label label-danger radius">否</span>';
    }
    //获取全部数据
    public function getAllList()
    {
        $data = self::get()->toArray();
        return $this->treeLevel($data);
    }

    /**
     * 获取层级数据
     * @param array $allow_node 用户拥有权限
     * @return array
     */
    public function treeData($allow_node)
    {
        $query = Node::where('is_menu','1');
        if(is_array($allow_node)){
            $query->whereIn('id',array_keys($allow_node));
        }
        $menuData = $query->get()->toArray();
        return $this->subTree($menuData);
    }
}
