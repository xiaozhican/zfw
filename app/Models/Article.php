<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Base
{
    //追加字段
    protected $appends = ['action'];

    public function getActionAttribute()
    {
        return $this->editBtn('admin.article.edit').'&nbsp'.$this->delBtn('admin.article.destroy');
    }
}
