<?php

namespace App\Http\Controllers\Admin;

use App\Models\Node;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    //后台首页显示
    public function index()
    {
        $auth = session('admin.auth');
        //读取菜单
        $menuData = (new Node())->treeData($auth);
        //dump($menuData);
        return view('admin.index.index',compact('menuData'));
    }

    public function welcome()
    {
        return view('admin.index.welcome');
    }

    public function logout()
    {
        auth()->logout();
        //跳转带提示
        return redirect(route('admin.login'))->with('success','退出成功，请重新登录');
    }
}
