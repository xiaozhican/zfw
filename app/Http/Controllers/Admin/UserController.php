<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Mail\Message;
use Mail;
use Hash;

class UserController extends BaseController
{
    //用户列表
    public function index()
    {
        $data = User::withTrashed()->paginate($this->pagesize);
        return view('admin.user.index',compact('data'));
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function store(Request $request)
    {
        $post = $this->validate($request,[
            'username'=>'required|unique:users,username',
            'truename'=>'required',
            'password'=>'required|confirmed',//两次密码是否一致
            'phone'=>'nullable|phone'
        ]);
        $data['password'] = bcrypt($post['password']);
        $data = $request->except(['_token','password_confirmation']);//排除字段
        $model = User::create($data);//添加入库
        $pwd = $post['password'];
        //发送邮件
        Mail::send('mail.useradd',compact('model','pwd'),function(Message $message)use($model){
            $message->to($model->email);
            $message->subject('添加用户邮件');
        });
        //跳转
        if(!$model){
            return redirect()->back()->with('error','添加用户失败');
        }
        return redirect(route('admin.user.index'))->with('success','添加用户成功');
    }
    //删除用户
    public function del(int $id)
    {
        $res = User::find($id)->delete();//软删除
        //User::find($id)->forceDelete();//强制删除
        if($res){
            return response()->json(['status'=>0,'msg'=>'删除成功']);
        }else{
            return response()->json(['status'=>1,'msg'=>'删除失败']);
        }
    }
    //还原删除
    public function restore(int $id)
    {
        User::onlyTrashed()->where('id',$id)->restore();
        return redirect(route('admin.user.index'))->with('success','还原成功');
    }
    //全选删除
    public function delall(Request $request){
        $ids = $request->get('id');
        User::destroy($ids);
        return response()->json(['status'=>0,'msg'=>'删除成功']);
    }
    //修改用户
    public function edit(int $id)
    {
        $model = User::find($id);
        return view('admin.user.edit',compact('model'));
    }

    public function update(Request $request,int $id)
    {
        $model = User::find($id);
        $spass = $request->get('spassword');//原密码
        $oldpass = $model->password;
        $bool = Hash::check($spass,$oldpass);//检查明文和密文是否一致
        //dump($bool);
        if($bool){
            //修改用户
            $post = $request->only([
                'truename','password','phone','email','sex'
            ]);
            if(!empty($post['password'])){
                $post['password'] = bcrypt($post['password']);
            }else{
                unset($post['password']);
            }
            $model->update($post);
            return redirect(route('admin.user.index'))->with('success','修改用户信息成功');
        }
        return redirect(route('admin.user.edit',$model))->withErrors(['error'=>'原密码不正确']);
    }
    //分配角色
    public function role(Request $request,User $user)
    {
        if($request->isMethod('post')){
            $post = $this->validate($request,[
                'role_id'=>'required|min:1'
            ],[
                'role_id'=>'必须选择一个'
            ]);
            $user->update($post);
            return redirect(route('admin.user.index'));
        }
        $roleAll = Role::all();
        return view('admin.user.role',compact('user','roleAll'));
    }
}
