<?php

namespace App\Http\Controllers\Admin;

use App\Models\Node;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use vendor\project\StatusTest;

class RoleController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //获取搜索框
        $name = $request->get('name','');
        //when 参数1：变量值存在，则执行匿名函数(参数2)，
        $data = Role::when($name,function($query)use ($name){
            $query->where('name','like',"%{$name}%");
        })->paginate($this->pagesize);
        //$data = Role::paginate($this->pagesize);
        return view('admin.role.index',compact('data','name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //接收所有数据
        //dd($request->all());
        try{
            $this->validate($request,[
                'name'=>'required|unique:roles,name'
            ]);
        }catch (\Exception $e){
            return ['status'=>1000,'msg'=>'验证不通过'];
        }
        Role::create($request->only('name'));
        return ['status'=>0,'msg'=>'添加角色成功'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $model = Role::find($id);
        return view('admin.role.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        try{
            $this->validate($request,[
                'name'=>'required|unique:roles,name,'.$id.',id'//排除id=$id的那行名为name的字段值
            ]);
        }catch (\Exception $e){
            return ['status'=>1000,'msg'=>'验证不通过'];
        }
        //修改
        Role::find($id)->update($request->only('name'));
        return response()->json(['status'=>0,'msg'=>'修改成功']);
    }
    //给角色分配权限
    public function node(Role $role)
    {
        //dump($role->nodes->toArray());
        $nodeAll = (new Node())->getAllList();//获取所有的权限
        //dump($nodeAll);
        //获取当前角色所拥有的权限
        $nodes = $role->nodes()->pluck('id')->toArray();
        return view('admin.role.node',compact('nodeAll','nodes','role'));
    }

    public function nodeSave(Request $request,Role $role)
    {
        //dump($request->all());
        $role->nodes()->sync($request->get('node'));//关联模型的数据同步
        return redirect(route('admin.role.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        //
    }
}
