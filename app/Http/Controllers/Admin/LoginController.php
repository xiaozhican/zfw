<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mews\Captcha\Captcha;

class LoginController extends Controller
{
    public function __construct()
    {
        //$this->middleware(['ckadmin:login']);
    }

    //登录显示
    public function index()
    {
        //判断是否登录了
        if(auth()->check()){
            return redirect(route('admin.index'));
        }
        return view('admin.login.login');
    }
    //登录验证
    public function login(Request $request)
    {
        //表单验证
        $post = $this->validate($request,[
            'username'=>'required',
            'password'=>'required',
            'code'=>'required'
        ]);
        if(Captcha::check($post['code'])){
            echo 111;
        }else{
            echo 222;
        }
        die;
        //登录
        $user = auth()->attempt($post);
        //dump($user);
        if($user){
            //$model = auth()->user();
            //登录成功
            //dump($model->toArray());
            //得到权限
            //判断是否是超级管理员
            if(config('rbac.super') != $post['username']){
                $userModel = auth()->user();
                $roleModel = $userModel->role;
                $nodeArr = $roleModel->nodes()->pluck('name','id')->toArray();
                session(['admin.auth'=>$nodeArr]);//权限保存在session中
                //dd($nodeArr);
            }else{
                session(['admin.auth'=>true]);//权限保存在session中
            }
            //跳转到后台
            return redirect(route('admin.index'));
        }else{
            return redirect(route('admin.login'))->withErrors(['error'=>'登录失败']);
        }
    }
}
