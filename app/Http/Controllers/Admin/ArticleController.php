<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AddArtRequest;
use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$data = Article::all();
        //return view('admin.article.index',compact('data'));
        if($request->header('X-Requested-With') == 'XMLHttpRequest'){
            //ajax请求
            //排序
            //$order = $request->get('order');
            ['column'=>$column,'dir'=>$dir] = $request->get('order')[0];
            //排序字段
            $orderField = $request->get('columns')[$column]['data'];

            $datamin = $request->get('datamin');//开始时间
            $datamax = $request->get('datamax');//结束时间
            $title = $request->get('title');//文章标题

            $query = Article::where('id','>',0);
            if(!empty($datamin) && !empty($datamax)){
                $datamin = date('Y-m-d H:i:s',strtotime($datamin.' 00:00:00'));
                $datamax = date('Y-m-d H:i:s',strtotime($datamax.' 23:59:59'));
                $query->whereBetween('created_at',[$datamin,$datamax]);
            }
            if(!empty($title)){
                $query->where('title','like',"%{$title}%");
            }
            //获取显示条数
            $start = $request->get('start',0);
            $length = min(100,$request->get('length',10));
            $total = $query->count();
            //获取数据
            $data = $query->orderBy($orderField,$dir)->offset($start)->limit($length)->get();
            $result = [
                'draw'=>$request->get('draw'),
                'recordsTotal'=>$total,
                'recordsFiltered'=>$total,
                'data'=>$data
            ];
            return $result;
        }
        return view('admin.article.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.article.create');
    }
    //webuploader图片上传
    public function upfile(Request $request)
    {
        //封面图片
        $pic = config('up.pic');
        if($request->hasFile('file')){
            $info = $request->file('file')->store('','article');
            $pic = '/uploads/article/'.$info;
        }
        return response()->json(['status'=>0,'url'=>$pic]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddArtRequest $request)
    {
        //封面图片
//        $pic = config('up.pic');
//        if($request->hasFile('pic')){
//            $info = $request->file('pic')->store('','article');
//            $pic = '/uploads/article/'.$info;
//        }
        $post = $request->except('_token','file');
        //$post['pic'] = $pic;
        Article::create($post);
        return redirect(route('admin.article.index'));
        //dump($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        return view('admin.article.edit',compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        //return response()->json(['status'=>0,''])
        $putData = $request->except(['action','created_at','updated_at','deleted_at','id']);
        $article->update($putData);
        return response()->json(['status'=>0,'url'=>route('admin.article.index')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();//软删除
        return response()->json(['id'=>1]);
    }
}
